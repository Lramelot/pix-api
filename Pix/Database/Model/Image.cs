﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Pix.Database.Model
{
    [Table("images")]
    public class Image
    {
        [Column("id")]
        public Guid Id { get; set; }

        [Column("upload_timestamp")]
        public DateTime UploadTimestamp { get; set; }

        [Column("size")]
        public long Size { get; set; }

        [Column("mime_type")]
        public string MimeType { get; set; }

        [Column("checksum")]
        public byte[] Checksum { get; set; }

        [NotMapped]
        public string HexChecksum
        {
            get
            {
                var hex = new StringBuilder(Checksum.Length * 2);
                foreach (byte b in Checksum)
                    hex.AppendFormat("{0:x2}", b);
                return hex.ToString();
            }
        }
    }
}
