﻿using Microsoft.EntityFrameworkCore;

namespace Pix.Database
{
    public class PixContext : DbContext
    {
        public PixContext(DbContextOptions<PixContext> options)
            : base(options)
        { }

        public virtual DbSet<Model.Image> Images { get; set; }
    }
}
