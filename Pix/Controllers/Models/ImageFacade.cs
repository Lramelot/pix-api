﻿using System;
using Pix.Database.Model;

namespace Pix.Controllers.Models
{
    public class ImageFacade
    {
        private readonly Image image;
        
        public ImageFacade(Image image) => this.image = image;

        public Guid Id { get => image.Id; }
        
        public DateTime UploadTimestamp { get => image.UploadTimestamp; }
        
        public long Size { get => image.Size; }
        
        public string MimeType { get => image.MimeType; }

        public string Checksum { get => image.HexChecksum; }
    }
}
