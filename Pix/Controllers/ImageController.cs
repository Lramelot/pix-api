﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pix.Controllers.Models;
using Pix.Database;
using Pix.Database.Model;
using Pix.Services;

namespace Pix.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly PixContext context;
        private readonly IBlobStorageService blobStorageService;

        public ImageController(
            IBlobStorageService blobStorageService,
            PixContext context)
        {
            // TODO potential refactor: moving blob service and db context interactions
            // into a dedicated business-oriented service

            this.blobStorageService = blobStorageService;
            this.context = context;
        }

        // GET api/image/f6cfa6f3-d81e-487b-82d3-2350741d2351
        [HttpGet("{id}/raw")]
        public IActionResult GetRawImage(Guid id)
        {
            // TODO using blob storage service as SSOT, will require DB then blob storage checks later on.
            if (!blobStorageService.Exists(id))
            {
                return NotFound();
            }

            var imageRow = context.Find<Image>(id);

            // Checking if file is valid (not corrupted, not tampered with)
            if (imageRow == null
                || !imageRow.Checksum.SequenceEqual(blobStorageService.GetChecksum(id)))
            {
                return NotFound();
            }
            
            return File(blobStorageService.GetStream(id), imageRow.MimeType);
        }

        [HttpGet("{id}")]
        public IActionResult GetImage(Guid id)
        {
            // TODO potential refactor: moving blob service and db context interactions
            // into a dedicated business-oriented service

            // TODO using blob storage service as SSOT, will require DB then blob storage checks later on.
            if (!blobStorageService.Exists(id))
            {
                return NotFound();
            }

            var imageRow = context.Find<Image>(id);
            return Ok(new ImageFacade(imageRow));
        }

        // POST api/image FormData { image = file }
        [HttpPost]
        public IActionResult UploadImage(IFormFile image)
        {
            // TODO potential refactor: moving blob service and db context interactions
            // into a dedicated business-oriented service

            // Persisting file on storage
            var id = blobStorageService.CreateDestinationBlob(out var stream);
            using (stream)
            {
                image.CopyTo(stream);
            }

            // Creating entity for database
            var imageRow = new Image
            {
                Id = id,
                Size = image.Length,
                MimeType = blobStorageService.GetMimeType(id),
                Checksum = blobStorageService.GetChecksum(id),
                UploadTimestamp = DateTime.UtcNow
            };
            context.Add(imageRow);
            context.SaveChanges();

            return CreatedAtAction("GetImage", new { imageRow.Id }, new ImageFacade(imageRow));
        }
    }
}
