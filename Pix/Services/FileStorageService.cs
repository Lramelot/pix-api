﻿using HeyRed.Mime;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Pix.Helpers;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Pix.Services
{
    public class FileStorageService : IBlobStorageService
    {
        private readonly string storagePath;

        public FileStorageService(IConfiguration appConfiguration)
        {
            storagePath = appConfiguration.GetValue<string>("StoragePath");
            if (!((File.GetAttributes(storagePath) & FileAttributes.Directory)
                 == FileAttributes.Directory))
            {
                throw new FileNotFoundException(storagePath);
            }

            // Appending prefix to path so our path will *always* end with a /
            storagePath = storagePath.EndsWith('/') ? storagePath : storagePath + '/';
        }

        public bool Exists(Guid id)
        {
            return File.Exists(storagePath + id);
        }

        public Stream GetStream(Guid id)
        {
            if (!Exists(id))
            {
                return null;
            }
            
            return File.OpenRead(storagePath + id);
        }

        public Guid CreateDestinationBlob(out Stream image)
        {
            var newId = GuidHelper.GenerateGuid();
            image = File.Create(storagePath + newId);

            return newId;
        }

        public string GetMimeType(Guid id)
        {
            string contentType = null;

            if (Exists(id))
            {
                contentType = MimeGuesser.GuessMimeType(storagePath + id);
            }

            return contentType;
        }

        public byte[] GetChecksum(Guid id)
        {
            using (var fs = new FileStream(storagePath + id, FileMode.Open))
            using (var bs = new BufferedStream(fs))
            {
                using (var sha1 = new SHA1Managed())
                {
                    return sha1.ComputeHash(bs);
                }
            }
        }
    }
}
