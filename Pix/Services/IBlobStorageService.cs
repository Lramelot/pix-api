﻿using System;
using System.IO;

namespace Pix.Services
{
    public interface IBlobStorageService
    {
        Guid CreateDestinationBlob(out Stream file);
        bool Exists(Guid id);
        Stream GetStream(Guid id);
        string GetMimeType(Guid id);
        byte[] GetChecksum(Guid id);
    }
}
