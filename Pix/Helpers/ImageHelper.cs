﻿using Microsoft.AspNetCore.Http;

namespace Pix.Helpers
{
    public class ImageHelper
    {
        public static bool IsImage(IFormFile file)
        {
            // TODO XXX Manually check the content type instead of trusting the user-sent header.
            // Will require to save the file to a temporary path before checking it.
            return file.ContentType.StartsWith("image/");
        }
    }
}
