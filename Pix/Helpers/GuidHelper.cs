﻿using System;

namespace Pix.Helpers
{
    public class GuidHelper
    {
        public static Guid GenerateGuid()
        {
            return Guid.NewGuid();
        }
    }
}
